package com.example.demo.student;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.LocalDate;
import java.time.Month;
import java.util.List;

@Configuration
public class StudentConfig {

    @Bean
    CommandLineRunner commandLineRunner(StudentRepository repository){
        return args -> {
            Student alan = new Student(
                    "Alan Chery",
                    LocalDate.of(1992, Month.JUNE,25),
                    "adotchery@gmail.com");
            Student sam = new Student(
                    "Sam Adams",
                    LocalDate.of(2009, Month.JANUARY,10),
                    "funwhorld@gmail.com");

            repository.saveAll(
                    List.of(alan, sam)
            );
        };
    }
}

